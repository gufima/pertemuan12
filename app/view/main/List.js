/**
 * This view is an example list of people.
 */
Ext.define('Latihan2.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Latihan2.store.Personnel'
    ],

    title: 'Data Anggota',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'id',  dataIndex: 'idd', width: 100 },
        { text: 'Name',  dataIndex: 'name', width: 230 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Phone', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'apakahini'
    }
});
