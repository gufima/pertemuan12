/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Latihan2.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    apakahini: function (sender, record) {
        var nama = record.data.name;
        var idd = record.data.idd;
        localStorage.setItem('nama', nama); //mendaftarkan storage
        localStorage.setItem('idd', idd);
        Ext.Msg.confirm('Confirm', 'Apakah Betul '+nama+'?', 'onConfirm', this);
        //alert(record)
        console.log(record.data);
    },

    onConfirm: function (choice) {
        var nama = localStorage.getItem('nama');
        var idd = localStorage.getItem('idd');
        if (choice === 'yes') {
            alert('Oke, '+nama+ '('+idd+')');
            //
        }
        else{
            alert('Tidak Oke, '+nama+ '('+idd+')');
        }
    },
    
    init: function() {
        var me = this,
            view = me.getView(),
            vm = this.getViewModel(), 
            progress;

        me._interval = setInterval(function() {
            if (view.isDestroyed) {
                clearInterval(me._interval);
                return;
            }
            progress = vm.get('progress');
            progress += 0.01;
            if (progress > 1) {
                progress = 0;
            }
            vm.set('progress', progress);
        }, 150);
    }
});
