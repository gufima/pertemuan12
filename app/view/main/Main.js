/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Latihan2.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',
    requires: [
        'Ext.MessageBox',

        'Latihan2.view.main.MainController',
        'Latihan2.view.main.MainModel',
        'Latihan2.view.main.List',
        'Latihan2.view.form.User',
        'Latihan2.view.group.Carousel',
        'Latihan2.view.sett.BasicDataView',
        'Latihan2.view.form.Login',
        'Latihan2.view.chart.Column',
        'Latihan2.view.chart.Pie',
        'Latihan2.view.chart.Scatter',
        'Latihan2.view.tree.TreePanel'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'right',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-feed',
            layout: 'fit',
            items: [{
                xtype: 'scatter-chart'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-tree',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        }
    ]
});
