Ext.define('Latihan2.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',
    requires: [
        'Ext.carousel.Carousel'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    /*defaults: {
        flex: 1
    },*/
    items: [{
        xtype: 'carousel',
        width: 400,
        direction: 'vertical',
        items: [{
            html: '<center><p><img src="img/acer1.jpg" alt="Acer Aspire 5" style="width:400px;height:350px;"></p></center>',
            cls: 'card'
        },
        {
            html: '<center><p><img src="img/hp15.jpg" alt="Acer Aspire 5" style="width:400px;height:350px;"></p></center>',
            cls: 'card'
        },
        {
            html: '<center><p><img src="img/nitro.png" alt="Acer Aspire 5" style="width:400px;height:350px;"></p></center>',
            cls: 'card'
        }]
    }, {
        xtype: 'spacer'
        },
    {
        xtype: 'carousel',
        flex: 1,
        ui: 'dark',
        items: [{
            html: '<center><p><img src="img/nitro.png" alt="Acer Aspire 5" style="width:400px;height:350px;"></p><c/enter>',
            cls: 'card dark'
        },
        {
            html: '<center><p><img src="img/hp15.jpg" alt="Acer Aspire 5" style="width:400px;height:350px;"></p><c/enter>',
            cls: 'card'
        },
        {
            html: '<center><p><img src="img/acer1.jpg" alt="Acer Aspire 5" style="width:400px;height:350px;"></p></center>',
            cls: 'card dark'
        }]
    }, {
        xtype: 'carousel',
        flex: 1,
        direction: 'vertical',
        ui: 'dark',
        items: [{
            html: '<center><p><img src="img/nitro.png" alt="Acer Aspire 5" style="width:400px;height:350px;"></p><c/enter>',
            cls: 'card dark'
        },
        {
            html: '<center><p><img src="img/hp15.jpg" alt="Acer Aspire 5" style="width:400px;height:350px;"></p><c/enter>',
            cls: 'card'
        },
        {
            html: '<center><p><img src="img/acer1.jpg" alt="Acer Aspire 5" style="width:400px;height:350px;"></p></center>',
            cls: 'card dark'
        }]
    }]
});