Ext.define('Latihan2.view.sett.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview', 
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Latihan2.store.Personnel',
        'Ext.field.Search'
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search Name',
                    name: 'searchfield',
                listeners: {
                        change: function (me, newValue, oldValue, eOpts){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('name', newValue);
                        }
                    }
                },
                {
                     xtype: 'spacer'
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search Email',
                    name: 'searchfield',
                listeners: {
                        change: function (me, newValue, oldValue, eOpts){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('email', newValue);
                        }
                    }
                },
                {
                     xtype: 'spacer'
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search Phone',
                    name: 'searchfield',
                listeners: {
                        change: function (me, newValue, oldValue, eOpts){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('phone', newValue);
                        }
                    }
                },
            ]
        },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '{photo}<br><font size=4 color="red">id : {idd}</font><br><b>Name :{name}</b><br><i>Email : {email}</i><br><u>Phone : {phone}</u><hr>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td></td><td>{photo}</td></tr>' +
                    '<tr><td>id: </td><td>{idd}</td></tr>' +
                    '<tr><td>Name: </td><td>{name}</td></tr>' + 
                    '<tr><td>Email: </td><td>{email}</td></tr>' +
                    '<tr><td>Phone: </td><td>{phone}</td></tr>'                
        }
    }]
});