Ext.define('Latihan2.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'name', 'email', 'phone', 'idd', 'photo', 'status', 'password', 'username'
    ],

    data: { items: [
        { photo: '<img src="resources/images/foto1.jpg" width="100px" height"75px">', idd: '12', name: 'Gusti Fiki M', email: "gustifiki@enterprise.com", phone: "0852-6570-6007", status: "Admin"},
        { photo: '<img src="resources/images/foto2.png" width="100px" height"75px">', idd: '13', name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "0823-222-2222", status: "Admin" },
        { photo: '<img src="resources/images/foto4.png" width="100px" height"75px">', idd: '14', name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333", status: "Member" },
        { photo: '<img src="resources/images/foto5.png" width="100px" height"75px">', idd: '15', name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444", status: "Member" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
